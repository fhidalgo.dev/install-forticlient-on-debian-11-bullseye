# Install Forticlient on Debian 11 bullseye

En Debian 11 bullseye hay un paquete que ha sido eliminado llamado libappindicator1. Este es utilizado para mostrar notificaciones. Sin embargo, existen aún muchos paquetes que lo tienen como dependencia, como por ejemplo el cliente de Forticlient para VPN.

Para resolver esa dependencia, lo que hay que hacer es simular el paquete con equivs:

1. Instalar equivs

`sudo apt-get install equivs`

2. Crear un archivo de control con la información del paquete.

`nano libappindicator1`

Dentro del archivo se coloca:

```
Section: misc
Priority: optional
Standards-Version: 3.9.2

Package: libappindicator1
Version: 2022
Description: dummy package of libappindicator1
```

3. Crear el paquete con equivs-build

`sudo equivs-build libappindicator1`

4. Instalar libappindicator1 con:

`sudo dpkg -i libappindicator1_2022_all.deb`

Ya con esto estaría instalado libappindicator1. Procedemos con forticlient

5. Descargar el archivo .deb

[Forticlient](https://www.fortinet.com/support/product-downloads#vpn)

6. Instalar dependencia

`sudo apt install libgconf-2-4`

7. Se instala forticlient

`sudo dpkg -i nombre_paquete`

8. En el caso de algún un error en cualquier momento de la instalación, se ejecuta:

`sudo apt --fix-broken install`

9. Si la simulación de libappindicator1 no funciona bien con Forticlient, se instala con dpkg los siguientes paquetes:

- http://ftp.de.debian.org/debian/pool/main/libi/libindicator/libindicator7_0.5.0-4_amd64.deb
- http://ftp.de.debian.org/debian/pool/main/liba/libappindicator/libappindicator1_0.4.92-7_amd64.deb

